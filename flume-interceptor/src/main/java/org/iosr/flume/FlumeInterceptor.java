package org.iosr.flume;

import org.apache.flume.Context;
import org.apache.flume.Event;
import org.apache.flume.interceptor.Interceptor;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by memorial on 12.05.14.
 */
public class FlumeInterceptor implements Interceptor{
    private static Logger LOGGER = Logger.getLogger("MyLogger");
    @Override
    public void initialize() {

    }

    @Override
    public Event intercept(Event event) {
        LOGGER.info("[Content of event body]" + event.getBody());
        Pattern regex = Pattern.compile("[^\\s\\[\"\\]']+|\"[^\"]*\"|\\[[^]]*]");
        Matcher regexMatcher = regex.matcher(new String(event.getBody()));
        StringBuilder stringBuilder = new StringBuilder();
        ArrayList<String> columnList = new ArrayList<String>();


        while (regexMatcher.find()) {
            columnList.add(regexMatcher.group().replaceAll("\"","").replaceAll("[\\||\\]|\\[]", "_"));
        }

        DateFormat dateFormat = new SimpleDateFormat("dd/MMM/yyyy:HH:mm:ssZ", Locale.US);
        Date date = null;
        try {

            date = dateFormat.parse(columnList.get(3).replaceAll("_", ""));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        columnList.set(3, String.valueOf(date.getTime()));

        for (String s : columnList){
            stringBuilder.append(s).append("|");
        }

        event.setBody(stringBuilder.toString().substring(0, stringBuilder.length()-1).getBytes());
        return event;
    }

    @Override
    public List<Event> intercept(List<Event> events) {
        for (Event e : events){
            intercept(e);
        }
        return events;
    }

    @Override
    public void close() {

    }
    public static class Builder implements Interceptor.Builder {

        @Override
        public Interceptor build() {
            return new FlumeInterceptor();
        }

        @Override
        public void configure(Context context) {

        }
    }
}
