package mr.dsl;

import mr.model.WritableLog;
import org.apache.hadoop.io.Text;

import static mr.dsl.Types.inTxt;
import static mr.dsl.Types.lonTxt;
import static mr.dsl.Types.txt;

/**
 * Created by Michal on 2014-05-24.
 */
public class Log {

    public static WritableLog unparseToObject(Text value) {
        String[] values = value.toString().split("\\|");

        int cnt = 0;
        for(String i: values) {
            System.out.println(cnt++ + ": "+i);
        }

        return new WritableLog(
                txt(values[0]),
                txt(values[4]),
                inTxt(values[5]),
                inTxt(values[6]),
                txt(values[8]),
                lonTxt(values[3])
        );
    }

}
