package mr.dsl;

import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;

/**
 * Created by Michal on 2014-05-24.
 */
public class Types {
    public static Text txt(String text) {
        return new Text(text);
    }

    public static IntWritable in(int intWrit) {
        return new IntWritable(intWrit);
    }

    public static IntWritable inTxt(String intWrit) {
        return new IntWritable(Integer.parseInt(intWrit));
    }

    public static LongWritable lon(long longWri) {
        return new LongWritable(longWri);
    }

    public static LongWritable lonTxt(String intWrit) {
        return new LongWritable(Long.parseLong(intWrit));
    }

    public static FloatWritable fl(float floatWritable) {
        return new FloatWritable(floatWritable);
    }

    public static FloatWritable flTxt(String floatWritable) {
        return new FloatWritable(Float.parseFloat(floatWritable));
    }
}
