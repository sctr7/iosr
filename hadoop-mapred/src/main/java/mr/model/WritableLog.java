package mr.model;


import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

/**
 * Created by Michal on 2014-05-23.
 */
public class WritableLog implements WritableComparable<WritableLog> {

    protected Text ip;
    protected Text rqLine;
    protected IntWritable statusCode;
    protected IntWritable dataSize;
    protected Text userAgent;
    protected LongWritable timestamp;

    public WritableLog(Text ip, Text rqLine, IntWritable statusCode, IntWritable dataSize, Text userAgent, LongWritable timestamp) {
        this.ip = ip;
        this.rqLine = rqLine;
        this.statusCode = statusCode;
        this.dataSize = dataSize;
        this.userAgent = userAgent;
        this.timestamp = timestamp;
    }

    public WritableLog() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof WritableLog)) return false;

        WritableLog that = (WritableLog) o;

        if (dataSize != null ? !dataSize.equals(that.dataSize) : that.dataSize != null) return false;
        if (ip != null ? !ip.equals(that.ip) : that.ip != null) return false;
        if (rqLine != null ? !rqLine.equals(that.rqLine) : that.rqLine != null) return false;
        if (statusCode != null ? !statusCode.equals(that.statusCode) : that.statusCode != null) return false;
        if (timestamp != null ? !timestamp.equals(that.timestamp) : that.timestamp != null) return false;
        if (userAgent != null ? !userAgent.equals(that.userAgent) : that.userAgent != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = ip != null ? ip.hashCode() : 0;
        result = 31 * result + (rqLine != null ? rqLine.hashCode() : 0);
        result = 31 * result + (statusCode != null ? statusCode.hashCode() : 0);
        result = 31 * result + (dataSize != null ? dataSize.hashCode() : 0);
        result = 31 * result + (userAgent != null ? userAgent.hashCode() : 0);
        result = 31 * result + (timestamp != null ? timestamp.hashCode() : 0);
        return result;
    }

    @Override
    public int compareTo(WritableLog that) {
        if(that.equals(this)) {
            return 0;
        } else {
            return this.ip.compareTo(that.ip);
        }
    }

    @Override
    public void write(DataOutput dataOutput) throws IOException {
        this.ip.write(dataOutput);
        this.rqLine.write(dataOutput);
        this.statusCode.write(dataOutput);
        this.dataSize.write(dataOutput);
        this.userAgent.write(dataOutput);
        this.timestamp.write(dataOutput);
    }

    @Override
    public void readFields(DataInput dataInput) throws IOException {
        this.ip = new Text(); this.ip.readFields(dataInput);
        this.rqLine = new Text(); this.rqLine.readFields(dataInput);
        this.statusCode = new IntWritable(); statusCode.readFields(dataInput);
        this.dataSize = new IntWritable(); this.dataSize.readFields(dataInput);
        this.userAgent = new Text(); this.userAgent.readFields(dataInput);
        this.timestamp = new LongWritable(); this.timestamp.readFields(dataInput);
    }

    @Override
    public String toString() {
        return "WritableLog{" +
                "ip=" + ip +
                ", rqLine=" + rqLine +
                ", statusCode=" + statusCode +
                ", dataSize=" + dataSize +
                ", userAgent=" + userAgent +
                '}';
    }

    public Text getIp() {
        return ip;
    }

    public void setIp(Text ip) {
        this.ip = ip;
    }

    public Text getRqLine() {
        return rqLine;
    }

    public void setRqLine(Text rqLine) {
        this.rqLine = rqLine;
    }

    public IntWritable getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(IntWritable statusCode) {
        this.statusCode = statusCode;
    }

    public IntWritable getDataSize() {
        return dataSize;
    }

    public void setDataSize(IntWritable dataSize) {
        this.dataSize = dataSize;
    }

    public Text getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(Text userAgent) {
        this.userAgent = userAgent;
    }

    public LongWritable getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LongWritable timestamp) {
        this.timestamp = timestamp;
    }

    public WritableLog ip(final Text ip) {
        this.ip = ip;
        return this;
    }

    public WritableLog rqLine(final Text rqLine) {
        this.rqLine = rqLine;
        return this;
    }

    public WritableLog statusCode(final IntWritable statusCode) {
        this.statusCode = statusCode;
        return this;
    }

    public WritableLog dataSize(final IntWritable dataSize) {
        this.dataSize = dataSize;
        return this;
    }

    public WritableLog userAgent(final Text userAgent) {
        this.userAgent = userAgent;
        return this;
    }

    public WritableLog timestamp(final LongWritable timestamp) {
        this.timestamp = timestamp;
        return this;
    }
}
