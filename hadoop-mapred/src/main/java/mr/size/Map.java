package mr.size;

import mr.model.WritableLog;
import mr.dsl.Log;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;


public class Map extends Mapper<LongWritable, Text, Text, WritableLog> {
    private final static IntWritable one = new IntWritable(1);
    private final Text word = new Text();

    @Override
    protected void map(LongWritable key, Text value, Context context)
            throws IOException, InterruptedException {
        System.out.println(">>>>>>>>>>>>> Map: " + key + " -> "
                + value.toString());

        WritableLog log = Log.unparseToObject(value);

        context.getConfiguration().set("mapreduce.textoutputformat.separator", ";");

        context.write(log.getIp(), log);
    }

		/*StringTokenizer tokenizer = new StringTokenizer(value.toString());
        while (tokenizer.hasMoreTokens()) {
			word.set(tokenizer.nextToken());
			context.write(word, one);
		}*/
}
