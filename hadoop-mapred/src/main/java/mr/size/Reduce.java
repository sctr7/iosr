package mr.size;

import mr.model.WritableLog;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

public class Reduce extends Reducer<Text, WritableLog, Text, Text> {

	@Override
	protected void reduce(Text key, Iterable<WritableLog> values,
			Context context) throws IOException, InterruptedException {
		System.out.print(">>>>>>>>>>>>> Reduce: " + values.toString() + " -> ");

        context.getConfiguration().set("mapreduce.textoutputformat.separator", ";");

		int sum = 0;
		for (WritableLog val : values) {
			sum+=val.getDataSize().get();
		}

		context.write(key, new Text(sum+";"+System.currentTimeMillis()));
	}

}
