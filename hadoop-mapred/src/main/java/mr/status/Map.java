package mr.status;

import java.io.IOException;

import mr.model.WritableLog;
import mr.dsl.Log;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;


public class Map extends Mapper<LongWritable, Text, IntWritable, WritableLog> {
    private final static IntWritable one = new IntWritable(1);
    private final Text word = new Text();

    @Override
    protected void map(LongWritable key, Text value, Context context)
            throws IOException, InterruptedException {
        System.out.println(">>>>>>>>>>>>> Map: " + key + " -> "
                + value.toString());

        WritableLog log = Log.unparseToObject(value);

        context.getConfiguration().set("mapreduce.textoutputformat.separator", ";");

        context.write(log.getStatusCode(), log);
    }

		/*StringTokenizer tokenizer = new StringTokenizer(value.toString());
        while (tokenizer.hasMoreTokens()) {
			word.set(tokenizer.nextToken());
			context.write(word, one);
		}*/
}
