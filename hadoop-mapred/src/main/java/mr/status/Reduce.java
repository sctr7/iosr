package mr.status;

import java.io.IOException;

import mr.model.WritableLog;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class Reduce extends Reducer<IntWritable, WritableLog, IntWritable, Text> {

	@Override
	protected void reduce(IntWritable key, Iterable<WritableLog> values,
			Context context) throws IOException, InterruptedException {
		System.out.print(">>>>>>>>>>>>> Reduce: " + values.toString() + " -> ");

        context.getConfiguration().set("mapreduce.textoutputformat.separator", ";");

		int sum = 0;
		for (WritableLog val : values) {
			sum++;
		}

		context.write(key,  new Text(sum+";"+System.currentTimeMillis()));
	}

}
