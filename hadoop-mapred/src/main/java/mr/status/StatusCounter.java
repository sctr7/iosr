package mr.status;

import mr.model.WritableLog;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

public class StatusCounter extends Configured implements Tool {

	public static void main(String[] args) throws Exception {
		FileSystem fs = FileSystem.get(new Configuration());
		fs.delete(new Path(args[1]), true);

		System.exit(ToolRunner.run(new StatusCounter(), args));
	}

	@Override
	public int run(String[] args) throws Exception {
		System.out.println(">>>>>>>>>>>>> Input directory: " + args[0]);
		System.out.println(">>>>>>>>>>>>> Output directory: " + args[1]);

		Job job = Job.getInstance(getConf(), "word_counter");
		job.setJarByClass(getClass());

		// configure input
		TextInputFormat.addInputPath(job, new Path(args[0]));
		job.setInputFormatClass(TextInputFormat.class);

		// configure map-reduce
		job.setMapperClass(Map.class);
        //job.setNumReduceTasks(0);
		//job.setCombinerClass(Reduce.class);
		job.setReducerClass(Reduce.class);

		// configure output
		TextOutputFormat.setOutputPath(job, new Path(args[1]));
		job.setOutputFormatClass(TextOutputFormat.class);
		job.setOutputKeyClass(IntWritable.class);
		job.setOutputValueClass(WritableLog.class);

		return job.waitForCompletion(true) ? 0 : 1;
	}

}
