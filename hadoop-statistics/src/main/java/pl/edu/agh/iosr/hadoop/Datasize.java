package pl.edu.agh.iosr.hadoop;

/**
 * Created by Michal
 * 2014-05-31.
 */

public class DataSize {

    String ipAddress;
    Integer dataSize;
    Long created;

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public Integer getDataSize() {
        return dataSize;
    }

    public void setDataSize(Integer dataSize) {
        this.dataSize = dataSize;
    }

    public Long getCreated() {
        return created;
    }

    public void setCreated(Long created) {
        this.created = created;
    }

    public DataSize(String ipAddress, Integer dataSize, Long created) {
        this.ipAddress = ipAddress;
        this.dataSize = dataSize;
        this.created = created;
    }

    @Override
    public String toString() {
        return "DataSize{" +
                "ipAddress='" + ipAddress + '\'' +
                ", dataSize=" + dataSize +
                ", created=" + created +
                '}';
    }
}
