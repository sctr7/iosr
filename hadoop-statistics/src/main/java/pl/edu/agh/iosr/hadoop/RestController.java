package pl.edu.agh.iosr.hadoop;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

/**
 * Created by SG0218822 on 6/1/2014.
 */
@Controller
public class RestController {

    @RequestMapping("/get-data")
    public @ResponseBody List<DataSize> getData(@RequestParam(value="from", required=false, defaultValue = "0") String from, @RequestParam(value="to", required=false, defaultValue = "0") String to) {
        SampleImpala impala = new SampleImpala();
        List<DataSize> byIpAddress = impala.findAllByIp(Long.parseLong(from), Long.parseLong(to));
        return byIpAddress;
    }

    @RequestMapping("/get-data-by-rq")
    public @ResponseBody List<DataSize> getDataByRq(@RequestParam(value="from", required=false, defaultValue = "0") String from, @RequestParam(value="to", required=false, defaultValue = "0") String to, @RequestParam(value="size", required=false, defaultValue = "0") String size) {
        SampleImpala impala = new SampleImpala();
        List<DataSize> byIpAddress = impala.findAllByResource(Long.parseLong(from), Long.parseLong(to), Long.parseLong(size));
        return byIpAddress;
    }

    @RequestMapping("/get-data-by-date")
    public @ResponseBody Map<String, Integer> getDataByDates(@RequestParam(value="from", required=false, defaultValue = "0") String from, @RequestParam(value="to", required=false, defaultValue = "0") String to) {
        SampleImpala impala = new SampleImpala();
        Map<String, Integer> byIpAddress = impala.findAllByDays(Long.parseLong(from), Long.parseLong(to));
        return byIpAddress;
    }

    @RequestMapping("/get-data-by-code-and-ip")
    public @ResponseBody
    Map<String, Map<Integer, Integer>> getDataByCodeAndIp(@RequestParam(value = "from", required = false, defaultValue = "0") String from, @RequestParam(value = "to", required = false, defaultValue = "0") String to) {
        SampleImpala impala = new SampleImpala();
        Map<String, Map<Integer, Integer>> byIpAddress = impala.findAllByIpAndStatusCode(Long.parseLong(from), Long.parseLong(to));
        return byIpAddress;
    }

}
