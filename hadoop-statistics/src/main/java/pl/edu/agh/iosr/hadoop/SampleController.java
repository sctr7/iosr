package pl.edu.agh.iosr.hadoop;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.*;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;


@Controller
@EnableAutoConfiguration
@ComponentScan
public class SampleController {

    @Autowired
    ConfigurableApplicationContext context;

    @RequestMapping("/")
    ModelAndView home(ModelMap model) {
        return new ModelAndView("index", model);
    }

    public static void main(String[] args) throws Exception {
        SpringApplication.run(SampleController.class, args);
    }
}