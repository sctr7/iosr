package pl.edu.agh.iosr.hadoop;

import org.apache.hadoop.hive.ql.metadata.Sample;

import java.sql.*;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by SG0218822 on 5/16/2014.
 */
public class SampleImpala {
    private static final String IMPALAD_HOST = "11.9.0.5";
    private static final String IMPALAD_JDBC_PORT = "21050";
    private static final String CONNECTION_URL = "jdbc:hive2://" + IMPALAD_HOST + ':' + IMPALAD_JDBC_PORT + "/;auth=noSasl";
    private static final String JDBC_DRIVER_NAME = "org.apache.hive.jdbc.HiveDriver";

    private Connection con = null;

    public SampleImpala() {
        try {
            Class.forName(JDBC_DRIVER_NAME);
            con = DriverManager.getConnection(CONNECTION_URL);
        } catch (ClassNotFoundException e1) {
            e1.printStackTrace();
        } catch (SQLException e1) {
            e1.printStackTrace();
        }
    }

    public void finalizeClass() {
        try {
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<DataSize> findAll() {

        try {
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM work.datasize");

            List<DataSize> list = new LinkedList<DataSize>();
            
            while (rs.next()) {
                list.add(new DataSize(rs.getString("ipaddress"), rs.getInt("datasize"), rs.getLong("created")));
            }

            return list;
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            finalizeClass();
        }
        return null;
    }

    public List<DataSize> findAllByIp(long from, long to) {
        try {
            StringBuilder where = new StringBuilder();
            where.append("1=1 ");
            if(from != 0) {
                where.append("AND logdate > ").append(from).append(" ");
            }

            if(to != 0) {
                where.append("AND logdate < ").append(to).append(" ");
            }

            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT SUM(size) as datasize, ipaddress, max(logdate) as created FROM work.logs WHERE "+where.toString()+" GROUP BY ipaddress");

            List<DataSize> list = new LinkedList<DataSize>();

            while (rs.next()) {
                list.add(new DataSize(rs.getString("ipaddress"), rs.getInt("datasize"), rs.getLong("created")));
            }
            return list;
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            finalizeClass();
        }
        return null;
    }

    public Map<String, Integer> findAllByDays(long from, long to) {
        try {
            StringBuilder where = new StringBuilder();
            where.append("1=1 ");
            if(from != 0) {
                where.append("AND logdate > ").append(from).append(" ");
            }

            if(to != 0) {
                where.append("AND logdate < ").append(to).append(" ");
            }

            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT SUM(size) as datasize, from_unixtime(round(logdate/1000), 'yyyy-MM-dd') as created FROM work.logs WHERE "+where.toString()+" GROUP BY created");

            Map<String, Integer> list = new HashMap<String, Integer>();

            while (rs.next()) {
                list.put(rs.getString("created"), rs.getInt("datasize"));
            }
            return list;
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            finalizeClass();
        }
        return null;
    }

    public List<DataSize> findAllByResource(long from, long to, long size) {
        try {
            StringBuilder where = new StringBuilder();
            where.append("1=1 ");
            if(from != 0) {
                where.append("AND logdate > ").append(from).append(" ");
            }

            if(to != 0) {
                where.append("AND logdate < ").append(to).append(" ");
            }

            StringBuilder more = new StringBuilder();
            more.append("HAVING datasize > ").append(size).append(" ");

            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT SUM(size) as datasize, requestline FROM work.logs WHERE "+where.toString()+" GROUP BY requestline "+more.toString()+" ORDER BY datasize DESC LIMIT 1000000");

            List<DataSize> list = new LinkedList<DataSize>();

            while (rs.next()) {
                list.add(new DataSize(rs.getString("requestline"), rs.getInt("datasize"), null));
            }
            return list;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            finalizeClass();
        }
        return null;
    }

    public Map<String, Map<Integer, Integer>> findAllByIpAndStatusCode(long from, long to) {
        try {
            StringBuilder where = new StringBuilder();
            where.append("1=1 ");
            if(from != 0) {
                where.append("AND logdate > ").append(from).append(" ");
            }

            if(to != 0) {
                where.append("AND logdate < ").append(to).append(" ");
            }

            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT count(httpstatuscode) as counter, httpstatuscode, ipaddress FROM work.logs WHERE "+where.toString()+" GROUP BY ipaddress, httpstatuscode");

            Map<String, Map<Integer, Integer>> list = new HashMap<String, Map<Integer, Integer>>();
            while (rs.next()) {
                Map<Integer, Integer> map;
                if((map = list.get(rs.getString("ipaddress"))) == null) {
                    map = new HashMap<>();
                    list.put(rs.getString("ipaddress"), map);
                }

                map.put(rs.getInt("httpstatuscode"), rs.getInt("counter"));
            }
            return list;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            finalizeClass();
        }
        return null;
    }
}
