$('#tab-stats').onTabShow(function () {
    var div = $('#chart_div');
    window.chart = new google.visualization.ColumnChart(div.get(0));

    drawVisitorsChart();
}, true);

// Handle viewport resizing
var previousWidth = $(window).width();
$(window).resize(function () {
    if (previousWidth != $(window).width()) {
        redrawChart();
        previousWidth = $(window).width();
    }
});

function redrawChart(data, title) {
    var div = $('#chart_div');

    window.title = title;

    chart.draw(data, {
        title: title?title:'Data transfer per ip',
        chartType: "LineChart",
        width: div.width(),
        height: 600,
        legend: 'right'});
}

function drawVisitorsChart() {
    // Create our data table.
    var data = new google.visualization.DataTable();
    window.datatable = data;

    data.addColumn("string", "Ip");
    data.addColumn("number", "DataSize");

    rows = [];

    var getData = {};
    if ($("#stats-period-from").val()) {
        getData.from = Date.parse($("#stats-period-from").val());
    }
    if ($("#stats-period-to").val()) {
        getData.to = Date.parse($("#stats-period-to").val());
    }

    $.get("/get-data", getData, function (r) {
        var tab = $('<table class="table"></table>');
        tab.append("<thead><tr><th>Id</th><th>Ip address</th><th>Data Size</th></tr></thead>");

        var res = eval(r);
        for (var i = 0; i < res.length; i++) {
            rows.push([res[i].ipAddress, res[i].dataSize / 1024 / 1024]);
            tab.append("<tr><th scope=\"row\">" + i + "</th><td>" + res[i].ipAddress + "</td><td>" + (res[i].dataSize / 1024 / 1024).toFixed(2) + "MB</td>")
        }
        $('#tab-infos').html(tab);

        data.addRows(rows);
        redrawChart(data, "Bandwidth usage per IP [in MB]");
    });
    // Message
    //notify('Chart updated');
}

function drawVisitorsChart3() {
    // Create our data table.
    var data = new google.visualization.DataTable();
    window.datatable = data;

    data.addColumn("string", "Rq line");
    data.addColumn("number", "DataSize");

    rows = [];

    var getData = {};
    if ($("#stats-period-from").val()) {
        getData.from = Date.parse($("#stats-period-from").val());
    }
    if ($("#stats-period-to").val()) {
        getData.to = Date.parse($("#stats-period-to").val());
    }

    if ($("#size").val()) {
        getData.size = Math.round($("#size").val() * 1024 * 1024);
    }

    $.get("/get-data-by-rq", getData, function (r) {
        var tab = $('<table class="table"></table>');
        tab.append("<thead><tr><th>Id</th><th>Rq line</th><th>Data Size</th></tr></thead>");

        var res = eval(r);
        for (var i = 0; i < res.length; i++) {
            rows.push([res[i].ipAddress, res[i].dataSize / 1024 / 1024]);
            tab.append("<tr><th scope=\"row\">" + i + "</th><td>" + res[i].ipAddress + "</td><td>" + (res[i].dataSize / 1024 / 1024).toFixed(2) + "MB</td>")
        }
        $('#tab-infos').html(tab);

        data.addRows(rows);
        redrawChart(data, "Bandwidth usage per resource [in MB]");
    });
    // Message
    //notify('Chart updated');
}

function drawVisitorsChart2() {
    // Create our data table.
    var data = new google.visualization.DataTable();
    window.datatable = data;

    data.addColumn("string", "Ip");
    data.addColumn("number", "DataSize");

    rows = [];

    var now = new Date();
    var from, to;

    var getData = {};
    if ($("#stats-period-from").val()) {
        getData.from = Date.parse($("#stats-period-from").val());
        from = new Date(getData.from);
    } else {
        from = new Date(now.getFullYear(), now.getMonth() - 1, now.getDay(), 0, 0, 0, 0);
    }

    if ($("#stats-period-to").val()) {
        getData.to = Date.parse($("#stats-period-to").val());
        to = new Date(getData.to);
    } else {
        to = now;
    }

    $.get("/get-data-by-date", getData, function (r) {
        var tab = $('<table class="table"></table>');
        tab.append("<thead><tr><th>Id</th><th>Date</th><th>Data Size</th></tr></thead>");

        var res = eval(r);
        var i = 0;
        while (from < to) {
            if (i++ > 100) break;
            var day = from.getFullYear() + "-" + (from.getMonth() < 9 ? "0" : "") + (from.getMonth() + 1) + "-" + (from.getDate() < 10 ? "0" : "") + from.getDate();
            size = res[day];
            if (size != undefined) {
                size = (size / 1024 / 1024).toFixed(2)
            } else {
                size = 0;
            }

            rows.push([day, Math.round(size)]);

            tab.append("<tr><th scope=\"row\">" + i + "</th><td>" + day + "</td><td>" + size + "MB</td>");
            from.setMilliseconds(from.getMilliseconds() + 1000 * 60 * 60 * 24);
        }
        $('#tab-infos').html(tab);

        data.addRows(rows);
        redrawChart(data, "Bandwidth usage per day [in MB]");
    });
    // Message
    //notify('Chart updated');
}

function drawVisitorsChart4() {
    // Create our data table.
    /*var data = new google.visualization.DataTable();
     */

    //data.addColumn("string", "Ip");
    //data.addColumn("number", "DataSize");

    var data = google.visualization.arrayToDataTable([
        ['Code', 'Bolivia', 'Ecuador', 'Madagascar', 'Papua New Guinea', 'Rwanda', 'Average'],
        ['2004/05', 165, 938, 522, 998, 450, 614.6],
        ['2005/06', 135, 1120, 599, 1268, 288, 682],
        ['2006/07', 157, 1167, 587, 807, 397, 623],
        ['2007/08', 139, 1110, 615, 968, 215, 609.4],
        ['2008/09', 136, 691, 629, 1026, 366, 569.6]
    ]);
    window.datatable = data;

    var now = new Date();
    var from, to;

    var getData = {};
    if ($("#stats-period-from").val()) {
        getData.from = Date.parse($("#stats-period-from").val());
        from = new Date(getData.from);
    } else {
        from = new Date(now.getFullYear(), now.getMonth() - 1, now.getDay(), 0, 0, 0, 0);
    }

    if ($("#stats-period-to").val()) {
        getData.to = Date.parse($("#stats-period-to").val());
        to = new Date(getData.to);
    } else {
        to = now;
    }

    $.get("/get-data-by-code-and-ip", getData, function (r) {
        var tab = $('<table class="table"></table>');
        tab.append("<thead><tr><th>Id</th><th>Ip address</th><th>Status code</th><th>Count</th></tr></thead>");

        var res = eval(r);
        var i = 0;

        var table = [];
        var codes = [];
        var codes2 = ["IP"];
        for (var ip in res) {
            if (res.hasOwnProperty(ip)) {
                for (var code in res[ip]) {
                    if (res[ip].hasOwnProperty(code)) {
                        if(codes.indexOf(code) == -1) {
                            codes.push(code);
                            codes2.push(code);
                        }
                        tab.append("<tr><th scope=\"row\">" + (++i) + "</th><td>" + ip + "</td><td>" + code + "</td><td>" + res[ip][code] + "</td></tr>");
                    }
                }
            }
        }
        table.push(codes2);

        var tmpTab;
        for (ip in res) {
            tmpTab = [];
            if (res.hasOwnProperty(ip)) {
                tmpTab.push(ip);
                for (code in codes) {
                    if(res[ip].hasOwnProperty(codes[code])) {
                        tmpTab.push(res[ip][codes[code]]);
                    } else {
                        tmpTab.push(0);
                    }
                }
            }
            table.push(tmpTab);
        }

        $('#tab-infos').html(tab);

        data = google.visualization.arrayToDataTable(table);
        window.datatable = data;

        redrawChart(data, "Request count per user and response code");
    });
    // Message
    //notify('Chart updated');
}

$("#modeChoose").change(function () {
    var t = $(this);
    var div = $('#chart_div');
    if (t.val() == "0") {
        window.chart = new google.visualization.ColumnChart(div.get(0));
    } else if (t.val() == "2") {
        window.chart = new google.visualization.PieChart(div.get(0));
    } else {
        window.chart = new google.visualization.LineChart(div.get(0));
    }

    redrawChart(datatable, title);
});

$("#stats-period-from, #stats-period-to, #size").change(function () {
    refFunction();
});

var refFunction = drawVisitorsChart;

$("#band1").click(function (e) {
    e.preventDefault();
    $('#size-field').hide();
    refFunction = drawVisitorsChart;
    drawVisitorsChart();
});

$("#band2").click(function (e) {
    e.preventDefault();
    $('#size-field').hide();
    refFunction = drawVisitorsChart2;
    drawVisitorsChart2();
});

$("#band4").click(function (e) {
    e.preventDefault();
    $('#size-field').show();
    refFunction = drawVisitorsChart3;
    drawVisitorsChart3();
});

$("#request1").click(function (e) {
    e.preventDefault();
    $('#size-field').hide();
    refFunction = drawVisitorsChart4;
    drawVisitorsChart4();
});