CREATE TABLE IF NOT EXISTS TMP.dataSize (
	IpAddress 	 						STRING,
	DataSize 							STRING,
	Created			 					STRING
) ROW FORMAT DELIMITED FIELDS TERMINATED BY ';';