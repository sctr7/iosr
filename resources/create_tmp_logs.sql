CREATE TABLE IF NOT EXISTS TMP.logs (
	IpAddress 	 						STRING,
	UserIdentifier 						STRING,
	UserId			 					STRING,
	LogDate			 					BIGINT,
	RequestLine		 					STRING,
	HttpStatusCode	 					STRING,
	Size			 					STRING,
	Referer			 					STRING,
	UserAgent		 					STRING
) ROW FORMAT DELIMITED FIELDS TERMINATED BY '|';