#!/bin/bash
export HADOOP_USER_NAME=admin;
impala-shell -q "LOAD DATA INPATH '/user/admin/ready/' OVERWRITE INTO TABLE TMP.logs;"
impala-shell -q "INSERT INTO work.logs SELECT 
	IpAddress,
	UserIdentifier,
	UserId,
	CAST (LogDate as BIGINT),
	RequestLine,
	CAST (HttpStatusCode as INT),
	CAST (Size as INT),
	Referer,
	UserAgent
FROM TMP.logs;"

impala-shell -q "LOAD DATA INPATH '/user/admin/mr/status-result/' OVERWRITE INTO TABLE TMP.statuscode;"
impala-shell -q "INSERT INTO work.statuscode SELECT 
	CAST (statusCode as INT),
	CAST (Counter as INT),
	CAST (Created as BIGINT)
FROM TMP.statuscode;"

impala-shell -q "LOAD DATA INPATH '/user/admin/mr/size-result/' OVERWRITE INTO TABLE TMP.datasize;"
impala-shell -q "INSERT INTO work.datasize SELECT 
	IpAddress,
	CAST (DataSize as INT),
	CAST (Created as BIGINT)
FROM TMP.datasize;"