#!/bin/bash
export HADOOP_USER_NAME=admin;

for i in $(hadoop fs -ls /user/admin/flume|awk '{print $8}')
do
        echo $i|grep -q '.tmp' || hadoop fs -mv $i /user/admin/ready
done

hadoop fs -rm -R -skipTrash /user/admin/mr/